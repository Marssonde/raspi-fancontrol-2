import os

class FilePath:

    def __init__(self, filename):
        self.filename = filename
        # Method to load parameters from a file or any other data source
        absFilePath = os.path.abspath(__file__)
        path, fn = os.path.split(absFilePath)
        self.filename=path+"/"+filename

    def getFilePath(self):
        return self.filename
     
