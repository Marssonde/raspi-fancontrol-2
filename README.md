# raspi-fancontrol-2

install raspbian os from:
https://www.raspberrypi.com/documentation/computers/getting-started.html
```
sudo crontab -e
```
```
@reboot pigpiod
```

Configure with the Python3 Script: Setup.py:  
```
python3 Setup.py  
```
Or write your confic into the "parameters.json"  
Example:  
parameters.json:  
```
{  
"GPIO_LED": 17, # if you have an LED on GPIO Port 17 (optional)  
"GPIO_FAN": 12, # The fan is connected to GPIO port 12 through a transistor (2N2222A)  
"frequency": 60, # The frequency is between 20Hz and 100Hz  
"THRESHOLD_MIN": 40, # The minimum temperature at which the fan turns on  
"THRESHOLD_MAX": 60, # The maximum temperature at which the fan receives 100% power  
"PULSEMODE_MIN": 60, # Minimum pulse width set when the THRESHOLD_MIN temperature is reached  
"PULSEMODE_MAX": 100, # Maximum pulse width set when the maximum temperature is reached (should always be 100%)  
"SLEEP_TIME": 60 # 60 seconds between the temperature measure
}  
```
These components are required for the fan control:   
1 transistor 2N2222A,   
1 resistor 1kOhm.   
Optionally a light-emitting idiode with a matching resistor.  
A circuit diagram is located in the images folder. The circuit diagram for the
 fan control is named raspyfan-pwm.png.  
   
![](/images/raspbyfan-pwm.png)

The operating principle of the fan is visible in the diagram: fan-control.png.
  
![](/images/fan-control.png)
start the script at boot with crontab  
```  
crontab -e   
```
```
@reboot python3 /home/{USER}/bin/fancontrol/fancontrol.py  
```  
let you see the temperature and pulsemode graph:  


```
./showTemperatureGraph.sh  
```
and:   
```
./showPulsemodeGraph.sh   
```




