import pigpio

class PWMController:

	def __init__(self, pin, frequency=1000):
		self.pi = pigpio.pi()
		self.pin = pin
		self.frequency = frequency
		# Pin als PWM-Ausgang festlegen
		self.pi.set_mode(self.pin, pigpio.OUTPUT)
		# PWM-Frequenz einstellen
		self.pi.set_PWM_frequency(self.pin, self.frequency)
        #self.pi.set_PWM_range(self.pin, 100)
    
	def set_duty_cycle_percent(self, duty_cycle_percent):
		# PWM-Duty-Cycle in Prozent umrechnen
		duty_cycle = int(duty_cycle_percent * 2.55)
        # Duty-Cycle setzen
		self.pi.set_PWM_dutycycle(self.pin, duty_cycle)
		
	def setLEDPin(self, pin):
	    self.ledpin = pin 
	    self.pi.set_mode(self.ledpin, pigpio.OUTPUT)
	    
	def setLEDOn(self):
	    self.pi.write(self.ledpin, 1)
	    
	def setLEDOff(self):
	    self.pi.write(self.ledpin, 0)
	    
	def cleanup(self):
	    self.pi.stop()
