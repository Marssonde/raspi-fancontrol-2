import os
import json
import subprocess
import pigpio
import time
from parametermanager import ParameterManager
from sqlitemanager import SQLiteManager
from filepath import FilePath
from pwmcontroller import PWMController

class Fancontrol:
 
    def __init__(self):
        self.loadParameter()
        self.pwm = PWMController(self.GPIO_FAN, self.frequency)  # Erstelle eine Instanz der PWMController-Klasse für Pin 12
        self.pwm.setLEDPin(self.GPIO_LED)
    
    def get_cpu_temperature(self):
        with open('/sys/class/thermal/thermal_zone0/temp', 'r') as file:
            self.temperature = float(file.read()) / 1000
        #print(self.temperature)

    def calcpulsemode(self):
        # Method to calculate the pulse mode based on the parameters
        self.get_cpu_temperature()
        # Errechnung der linearen Funktion y = m*x +c
        m=(self.PULSEMODE_MAX-self.PULSEMODE_MIN)/(self.THRESHOLD_MAX-self.THRESHOLD_MIN)
        # c = y - m*x
        c=((self.PULSEMODE_MAX)-(m*self.THRESHOLD_MAX))
        # y= m*x+c => der lineare Graph zur Berechnung der Pulsweite 
        self.pulsemode=int(m*self.temperature+c)
        if self.temperature >= self.THRESHOLD_MAX:
            self.pulsemode = self.PULSEMODE_MAX
        if self.temperature <= self.THRESHOLD_MIN:
            self.pulsemode=0
        #print(self.temperature)
        #print(self.pulsemode)

        
    def loadParameter(self):
        paramsfilename = FilePath("parameters.json")
        filename = paramsfilename.getFilePath()
        # Create an instance of ParameterManager with the desired filename
        parameter_manager = ParameterManager(filename)
        # Load the parameters from the JSON file
        loaded_parameters = parameter_manager.load_parameters()
        # Access individual parameters
        self.GPIO_LED = loaded_parameters['GPIO_LED']
        self.GPIO_FAN = loaded_parameters['GPIO_FAN']
        self.frequency = loaded_parameters['frequency']
        self.THRESHOLD_MIN = loaded_parameters['THRESHOLD_MIN']
        self.THRESHOLD_MAX = loaded_parameters['THRESHOLD_MAX']
        self.PULSEMODE_MIN = loaded_parameters['PULSEMODE_MIN']
        self.PULSEMODE_MAX = loaded_parameters['PULSEMODE_MAX']
        self.SLEEP_TIME = loaded_parameters['SLEEP_TIME']
        #print(loaded_parameters)
        
    def setGPIOFanPWM(self):
        self.calcpulsemode()
        fname = FilePath("setpulsemode.sh")
        filename = fname.getFilePath()
        # Create an instance of ParameterManager with the desired filename
        # subprocess.run([filename, ""+str(self.GPIO_FAN)+"" , ""+str(self.frequency)+"" , ""+str(self.pulsemode*10000)+"" ])
        #print(self.pulsemode)
        if self.pulsemode>=0:
            self.pwm.set_duty_cycle_percent(self.pulsemode)  # Setze den Duty-Cycle auf ... Prozent
        #print(pulsemode)
        #print(temperature)
        sql_manager = SQLiteManager()
        sql_manager.saveValues(self.temperature, self.pulsemode)
        self.pwm.setLEDOn()
        time.sleep(3)
        self.pwm.setLEDOff()
        
    def getSleepTime(self):
        return self.SLEEP_TIME
          
fan_control = Fancontrol()
# Main loop to monitor CPU temperature and control fan speed

while True:
    fan_control.setGPIOFanPWM()
    time.sleep(fan_control.getSleepTime())
    
        
