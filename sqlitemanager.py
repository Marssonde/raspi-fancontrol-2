import os
import sys, getopt
import datetime
import sqlite3
from sqlite3 import Error
from filepath import FilePath

class SQLiteManager:
    dbfilename = "fancontrol.db"

    def create_connection(self):
        """ create a database connection to the SQLite database
            specified by db_file
        :param db_file: database file
        :return: Connection object or None
        """
        dbfilepath = FilePath(self.dbfilename)
        filename = dbfilepath.getFilePath()
        conn = None
        if not os.path.exists(filename):
            try:
                conn = sqlite3.connect(filename)
                if conn is not None:
                    self.create_table(conn)
                    conn.close()
                else:
                    print("Error! cannot create the database connection.")
            except Error as e:
                print("Error! cannot create the database connection.")
        conn = None
        try:
            conn = sqlite3.connect(filename)
            return conn
        except Error as e:
            if conn is not None:
                self.create_table(conn)
                return conn
            else:
                print("Error! cannot create the database connection.")
        return conn
        
    def create_table(self, conn):
        """ create a table from the create_table_sql statement
        :param conn: Connection object
        :param create_table_sql: a CREATE TABLE statement
        """
        sql_create_main_table = """ CREATE TABLE MAIN (
                                ID INTEGER PRIMARY KEY AUTOINCREMENT,
                                TIMESTAMP INTEGER ,
                                TEMPERATURE FLOAT ,
                                PULSEMODE INTEGER
                                ); """
        try:
            cur = conn.cursor()
            cur.execute(sql_create_main_table)
            cur.close()
        except Error as e:
            print(e)
        
    def createDatabase(self):

        # create a database connection
        conn = self.create_connection()

        # create tables
        if conn is not None:
            # create projects table
            self.create_table(conn)
            conn.close()
        else:
            print("Error! cannot create the database connection.")
            
    def loadTemperatureValues(self, hour):
        try:
            # create a database connection
            conn = self.create_connection()
            sql_select_query = "SELECT TIMESTAMP, TEMPERATURE from MAIN WHERE TIMESTAMP BETWEEN strftime('%s','now', '-"+hour+" hour') AND strftime('%s','now');"

            cur = conn.cursor()
            cur.execute(sql_select_query)
            rows = cur.fetchall()
            cur.close()
            conn.close()
            return rows
        except sqlite3.Error as error:
            print("Failed to read data from sqlite table", error)
            return None
       
    def loadPulsemodeValues(self, hour):
        try:
            # create a database connection
            conn = self.create_connection()
            sql_select_query = "SELECT TIMESTAMP, PULSEMODE from MAIN WHERE TIMESTAMP BETWEEN strftime('%s','now', '-"+hour+" hour') AND strftime('%s','now');"

            cur = conn.cursor()
            cur.execute(sql_select_query)
            rows = cur.fetchall()
            cur.close()
            conn.close()
            return rows
        except sqlite3.Error as error:
            print("Failed to read data from sqlite table", error)
            return None 
                
    def saveValues(self, temp, pmode):
        # create a database connection
        conn = self.create_connection()
        sql = "INSERT INTO MAIN (TIMESTAMP,TEMPERATURE,PULSEMODE) VALUES (strftime('%s','now'),?,?);"     
        data_tuple = (temp, pmode)
        cur = conn.cursor()
        cur.execute(sql, data_tuple)
        conn.commit()
        cur.close()
        conn.close()
        return cur.lastrowid

