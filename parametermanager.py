import json

class ParameterManager:
    def __init__(self, filename):
        self.filename = filename

    def save_parameters(self, parameters):
        with open(self.filename, 'w') as file:
            json.dump(parameters, file)

    def load_parameters(self):
        try:
            with open(self.filename, 'r') as file:
                parameters = json.load(file)
                return parameters
        except FileNotFoundError:
            # If the file doesn't exist, return an empty dictionary
            return {}

