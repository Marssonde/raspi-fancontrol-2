import os
import psutil
import datetime
import matplotlib.pyplot as plt
import numpy as np
from sqlitemanager import SQLiteManager

def convert_timestamp(val):
    """Convert Unix epoch timestamp to datetime.datetime object."""
    return datetime.datetime.fromtimestamp(int(val))
   
sqlman = SQLiteManager()
while True:
   data1=[]
   data2=[]
   records = sqlman.loadPulsemodeValues("1")
   for row in records:
            data1.append(convert_timestamp(row[0]))
            data2.append(row[1])
   plt.plot(data1,data2)
   plt.pause(30)
