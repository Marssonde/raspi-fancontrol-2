import os
import json
from parametermanager import ParameterManager
import tkinter as tk
from tkinter import *
from tkinter import ttk

def check_tkinter():
    try:
        root = tk.Tk()
    except:
        return False
    else:
        root.destroy()
        return True

def calculate(*args):
    try:
        # Method to save parameters to a file or any other storage
        parameters = {
            'GPIO_LED': int(gpioled.get()),
            'GPIO_FAN': int(gpiofan.get()),
            'frequency': int(frequency.get()),
            'THRESHOLD_MIN': int(threshold_min.get()),
            'THRESHOLD_MAX': int(threshold_max.get()),
            'PULSEMODE_MIN': int(pulsemode_min.get()),
            'PULSEMODE_MAX': int(pulsemode_max.get()),
            'SLEEP_TIME': int(sleeptime.get())
        }
        absFilePath = os.path.abspath(__file__)
        path, fname = os.path.split(absFilePath)
        filename=path+"/"+"parameters.json"
        # Create an instance of ParameterManager with the desired filename
        parameter_manager = ParameterManager(filename)
        # Save the parameters to the JSON file
        parameter_manager.save_parameters(parameters)
    except ValueError:
        pass


def createDialog():
    global GPIO_LED
    global GPIO_FAN
    global freq
    global THRESHOLD_MIN
    global THRESHOLD_MAX
    global PULSEMODE_MIN
    global PULSEMODE_MAX
    global SLEEP_TIME
    global gpiofan
    global gpioled
    global frequency
    global threshold_min
    global threshold_max
    global pulsemode_min
    global pulsemode_max
    global sleeptime
    root = Tk()
    root.title("Fancontrol Config")
    mainframe = ttk.Frame(root, padding="3 3 12 12")
    mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)
    # gpio fan
    gpiofan = StringVar()
    gpiofan.set(GPIO_FAN)
    gpiofan_entry = ttk.Entry(mainframe, width=7, textvariable=gpiofan)
    gpiofan_entry.grid(column=2, row=1, sticky=(W, E))
    # gpio led
    gpioled = StringVar()
    gpioled.set(GPIO_LED)
    gpioled_entry = ttk.Entry(mainframe, width=7, textvariable=gpioled)
    gpioled_entry.grid(column=2, row=2, sticky=(W, E))
    # Frequency
    frequency = StringVar()
    frequency.set(freq)
    frequency_entry = ttk.Entry(mainframe, width=7, textvariable=frequency)
    frequency_entry.grid(column=2, row=3, sticky=(W, E))
    # THRESHOLD_MIN
    threshold_min = StringVar()
    threshold_min.set(THRESHOLD_MIN)
    threshold_min_entry = ttk.Entry(mainframe, width=7, textvariable=threshold_min)
    threshold_min_entry.grid(column=2, row=4, sticky=(W, E))
    # THRESHOLD_MAX
    threshold_max = StringVar()
    threshold_max.set(THRESHOLD_MAX)
    threshold_max_entry = ttk.Entry(mainframe, width=7, textvariable=threshold_max)
    threshold_max_entry.grid(column=2, row=5, sticky=(W, E))
    # PULSEMODE_MIN
    pulsemode_min = StringVar()
    pulsemode_min.set(PULSEMODE_MIN)
    pulsemode_min_entry = ttk.Entry(mainframe, width=7, textvariable=pulsemode_min)
    pulsemode_min_entry.grid(column=2, row=6, sticky=(W, E))
    # PULSEMODE_MAX
    pulsemode_max = StringVar()
    pulsemode_max.set(PULSEMODE_MAX)
    pulsemode_max_entry = ttk.Entry(mainframe, width=7, textvariable=pulsemode_max)
    pulsemode_max_entry.grid(column=2, row=7, sticky=(W, E))
    # SLEEP_TIME
    sleeptime = StringVar()
    sleeptime.set(SLEEP_TIME)
    sleeptime_entry = ttk.Entry(mainframe, width=7, textvariable=sleeptime)
    sleeptime_entry.grid(column=2, row=8, sticky=(W, E))
    #
    ttk.Button(mainframe, text="Save", command=calculate).grid(column=3, row=9, sticky=W)
    ttk.Label(mainframe, text="Fan GPIO Pin:").grid(column=1, row=1, sticky=W)
    ttk.Label(mainframe, text="LED GPIO Pin:").grid(column=1, row=2, sticky=W)
    ttk.Label(mainframe, text="Frequency:").grid(column=1, row=3, sticky=W)
    ttk.Label(mainframe, text="THRESHOLD_MIN:").grid(column=1, row=4, sticky=W)
    ttk.Label(mainframe, text="THRESHOLD_MAX:").grid(column=1, row=5, sticky=W)
    ttk.Label(mainframe, text="PULSEMODE_MIN:").grid(column=1, row=6, sticky=W)
    ttk.Label(mainframe, text="PULSEMODE_MAX:").grid(column=1, row=7, sticky=W)
    ttk.Label(mainframe, text="SLEEP_TIME:").grid(column=1, row=8, sticky=W)
    for child in mainframe.winfo_children(): 
        child.grid_configure(padx=5, pady=5)
    gpiofan_entry.focus()
    root.bind("<Return>", calculate)
    root.mainloop()

def createShell():
    global GPIO_LED
    global GPIO_FAN
    global freq
    global THRESHOLD_MIN
    global THRESHOLD_MAX
    global PULSEMODE_MIN
    global PULSEMODE_MAX
    global SLEEP_TIME
    print("GPIO_LED: "+str(GPIO_LED))
    print("GPIO_FAN: "+str(GPIO_FAN))
    print("frequency: "+str(freq))
    print("THRESHOLD_MIN: "+str(THRESHOLD_MIN))
    print("THRESHOLD_MAX: "+str(THRESHOLD_MAX))
    print("PULSEMODE_MIN: "+str(PULSEMODE_MIN))
    print("PULSEMODE_MAX: "+str(PULSEMODE_MAX))
    print("SLEEP_TIME: "+str(SLEEP_TIME))
    GPIO_LED = int(input("Bitte geben Sie den Wert für GPIO_LED ein: "))
    GPIO_FAN = int(input("Bitte geben Sie den Wert für GPIO_FAN ein: "))
    frequency = int(input("Bitte geben Sie den Wert für frequency ein: "))
    THRESHOLD_MIN = int(input("Bitte geben Sie den Wert für THRESHOLD_MIN ein: "))
    THRESHOLD_MAX = int(input("Bitte geben Sie den Wert für THRESHOLD_MAX ein: "))
    PULSEMODE_MIN = int(input("Bitte geben Sie den Wert für PULSEMODE_MIN ein: "))
    PULSEMODE_MAX = int(input("Bitte geben Sie den Wert für PULSEMODE_MAX ein: "))
    SLEEP_TIME = int(input("Bitte geben Sie den Wert für SLEEP_TIME ein: "))
    # Method to save parameters to a file or any other storage
    parameters = {
            'GPIO_LED': int(GPIO_LED),
            'GPIO_FAN': int(GPIO_FAN),
            'frequency': int(frequency),
            'THRESHOLD_MIN': int(THRESHOLD_MIN),
            'THRESHOLD_MAX': int(THRESHOLD_MAX),
            'PULSEMODE_MIN': int(PULSEMODE_MIN),
            'PULSEMODE_MAX': int(PULSEMODE_MAX),
            'SLEEP_TIME' : int(SLEEP_TIME)
    }
    absFilePath = os.path.abspath(__file__)
    path, fname = os.path.split(absFilePath)
    filename=path+"/"+"parameters.json"
    # Create an instance of ParameterManager with the desired filename
    parameter_manager = ParameterManager(filename)
    # Save the parameters to the JSON file
    parameter_manager.save_parameters(parameters)
  
absFilePath = os.path.abspath(__file__)
path, fname = os.path.split(absFilePath)
filename=path+"/"+"parameters.json"
# Create an instance of ParameterManager with the desired filename
parameter_manager = ParameterManager(filename)
# Load the parameters from the JSON file
loaded_parameters = parameter_manager.load_parameters()
if loaded_parameters == {}:
    loaded_parameters = {
            'GPIO_LED': 0,
            'GPIO_FAN': 0,
            'frequency': 0,
            'THRESHOLD_MIN': 0,
            'THRESHOLD_MAX': 0,
            'PULSEMODE_MIN': 0,
            'PULSEMODE_MAX': 0,
            'SLEEP_TIME' : 0
    }
# Access individual parameters
GPIO_LED = loaded_parameters['GPIO_LED']
GPIO_FAN = loaded_parameters['GPIO_FAN']
freq = loaded_parameters['frequency']
THRESHOLD_MIN = loaded_parameters['THRESHOLD_MIN']
THRESHOLD_MAX = loaded_parameters['THRESHOLD_MAX']
PULSEMODE_MIN = loaded_parameters['PULSEMODE_MIN']
PULSEMODE_MAX = loaded_parameters['PULSEMODE_MAX']  
SLEEP_TIME = loaded_parameters['SLEEP_TIME'] 
if check_tkinter() == True:
    createDialog()
else:
    createShell()
