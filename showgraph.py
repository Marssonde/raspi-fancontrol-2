import os
import psutil
import datetime
import plotly
import plotly.graph_objs as go
from sqlitemanager import SQLiteManager
def convert_timestamp(val):
    """Convert Unix epoch timestamp to datetime.datetime object."""
    return datetime.datetime.fromtimestamp(int(val))

data1=[]
data2=[]
sqlman = SQLiteManager()
records = sqlman.loadValues("3")
for row in records:
            data1.append(convert_timestamp(row[0]))
            data2.append(row[1])
data = [go.Scatter(
    x=data1,
    y=data2)]
plotly.offline.plot(data)
